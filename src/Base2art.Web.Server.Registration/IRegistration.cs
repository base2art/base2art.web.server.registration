﻿namespace Base2art.Web.Server.Registration
{
    using System;
    using ComponentModel.Composition;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;

    public interface IRegistration : IDisposable
    {
        void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config);

        void RegisterSystemServicesPostUserService(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config);

        void RegisterUserServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config);

        void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config);

        void RegisterMiddleware(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config);

        void RegisterApiExploration(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config);

        void RegisterCors(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config);

        void RegisterBindings(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config);

        void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config);

        void RegisterFormatters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config);

        void RegisterRoutes(IRouteManager router, IServiceProvider services, IServerConfiguration config);
    }
}