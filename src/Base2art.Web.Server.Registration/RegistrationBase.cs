﻿namespace Base2art.Web.Server.Registration
{
    using System;
    using System.Collections.Generic;
    using ComponentModel.Composition;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;

    public class RegistrationBase : IRegistration
    {
        public virtual void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
        }

        public virtual void RegisterSystemServicesPostUserService(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services,
                                                                  IServerConfiguration config)
        {
        }

        public virtual void RegisterUserServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
        }

        public virtual void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
        }

        public virtual void RegisterMiddleware(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
        }

        public virtual void RegisterApiExploration(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
        }

        public virtual void RegisterCors(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
        }

        public virtual void RegisterBindings(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
        }

        public virtual void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
        }

        public virtual void RegisterFormatters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
        }

        public virtual void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~RegistrationBase()
        {
            // Finalizer calls Dispose(false)  
            this.Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        protected static IDictionary<string, object> ToDictionary(IReadOnlyDictionary<string, object> modelBindingBinderProperties)
        {
            var dictionary = new Dictionary<string, object>();

            if (modelBindingBinderProperties == null)
            {
                return dictionary;
            }

            foreach (var key in modelBindingBinderProperties.Keys)
            {
                dictionary[key] = modelBindingBinderProperties[key];
            }

            return dictionary;
        }
    }
}