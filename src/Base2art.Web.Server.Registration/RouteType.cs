namespace Base2art.Web.Server.Registration
{
    public enum RouteType
    {
        Endpoint,
        Task,
        HealthCheck,
        UI,
        Special
    }
}