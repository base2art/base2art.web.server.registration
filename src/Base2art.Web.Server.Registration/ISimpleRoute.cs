﻿namespace Base2art.Web.Server.Registration
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Reflection;

    public interface ISimpleRoute
    {
        HttpMethod Method { get; }

        string RouteTemplate { get; }

        Type Controller { get; }

        MethodInfo Action { get; }

        string Tag { get; }

        string Id { get; }

        IReadOnlyDictionary<string, object> Parameters { get; }

        IReadOnlyDictionary<string, object> Properties { get; }

        RouteType RouteType { get; }
    }
}