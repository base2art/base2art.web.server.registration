﻿namespace Base2art.Web.Server.Registration
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Reflection;
    using Microsoft.AspNetCore.Routing;

    public interface IRouteManager
    {
        IEnumerable<ISimpleRoute> Routes { get; }

        void MapRoute(
            HttpMethod method,
            string routeTemplate,
            Type controller,
            MethodInfo action,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType);

        void MapRoute(
            HttpMethod method,
            string routeTemplate,
            Type controller,
            MethodInfo action,
            string tag,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType);

        void Insert(int index, IRouter router);
    }
}